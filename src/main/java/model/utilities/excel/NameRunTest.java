package model.utilities.excel;

import java.util.List;

public class NameRunTest {

	private InteractWithExcel iwe;
	private List<String> nameRunTest;
	
	public NameRunTest(String folderName, String testName) {
		iwe = new InteractWithExcel(folderName, testName);
	}
	
	public List<String> getNameRunTest(){
		try {
			nameRunTest = iwe.nameRunTests("RunTest","Test");
			
		} catch (Exception e) {
			System.out.println("N�o foi poss�vel obter os nomes dos testes!" + e.getMessage());
		}
		return nameRunTest;
		
	}
	
}
