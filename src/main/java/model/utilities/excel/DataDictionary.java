package model.utilities.excel;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class DataDictionary {

	public HashMap<String, Object> dictionary;
	protected InteractWithExcel iwe;

	public DataDictionary(String folderName, String testName) {
		iwe = new InteractWithExcel(folderName, testName);
	}

	public List<HashMap<String, Object>> getDictionary() {
		List<HashMap<String, Object>> excelList = new ArrayList<HashMap<String, Object>>();
		try {
			excelList = iwe.runTestInData("RunTest","Test");

		} catch (Exception e) {
			System.out.println("N�o foi poss�vel obter o dicion�rio de testes da planilha!" + e.getMessage());
		}
		return excelList;

	}

}
