package model.utilities;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import org.json.simple.JSONObject;

public class Utils {

	/********* Console Log *********/
	public void description(String text) {
		System.out.println(text);
	}

	/****** Cria arquivo Json ******/

	public JSONObject createJsonFile(String nome, String cpf, String email, String valor, String parcelas,
			String seguro) {
		Number number = Double.parseDouble(valor);
		JSONObject request = new JSONObject();
		request.put("nome", nome);
		request.put("cpf", cpf);
		request.put("email", email);
		request.put("valor", number.doubleValue());
		request.put("parcelas", Integer.parseInt(parcelas));
		request.put("seguro", Boolean.parseBoolean(seguro));

		return request;
	}
	
	public JSONObject createJsonFile(String nome) {
		JSONObject request = new JSONObject();
		request.put("nome", nome);

		return request;
	}
	
	public JSONObject createJsonFileJustWithSafe(String seguro) {
		JSONObject request = new JSONObject();
		request.put("seguro", seguro);

		return request;
	}

	/***** Working with String *****/

	public String[] returnArrayOfExcel(String valuesOfExcel) {

		return valuesOfExcel.trim().split(",");

	}

	/************ Date ************/

	public Date obtainedDateWithDifferenceOfDays(int differenceOfDays) throws Exception {
		Calendar calendar = Calendar.getInstance();
		calendar.add(Calendar.DAY_OF_MONTH, differenceOfDays);
		return calendar.getTime();

	}

	public String obtainedDateFormated(Date date) throws Exception {
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		return sdf.format(date);

	}

	public String obtainedDateWithHoursFormated(Date date) throws Exception {
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		return sdf.format(date);

	}

}
