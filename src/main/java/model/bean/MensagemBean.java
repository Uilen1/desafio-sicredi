package model.bean;

public class MensagemBean {

	private String mensagem;
	
	public MensagemBean() {
		
	}

	public MensagemBean(String mensagem) {
		super();
		this.mensagem = mensagem;
	}
	
	public String getMensagem() {
		return mensagem;
	}

	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}

	@Override
	public String toString() {
		return "MensagemBean [mensagem=" + mensagem + "]";
	}

	
	
}
