package model.bean;

public class ErrorBean {

	private String nome;
	private String cpf;
	private String email;
	private String valor;
	private String parcelas;
	private String seguro;

	public ErrorBean() {

	}
	
	public ErrorBean(String nome, String cpf, String email, String valor, String parcelas, String seguro) {
		super();
		this.nome = nome;
		this.cpf = cpf;
		this.email = email;
		this.valor = valor;
		this.parcelas = parcelas;
		this.seguro = seguro;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getValor() {
		return valor;
	}

	public void setValor(String valor) {
		this.valor = valor;
	}

	public String getParcelas() {
		return parcelas;
	}

	public void setParcelas(String parcelas) {
		this.parcelas = parcelas;
	}

	public String getSeguro() {
		return seguro;
	}

	public void setSeguro(String seguro) {
		this.seguro = seguro;
	}

	@Override
	public String toString() {
		return "ErrorBean [nome=" + nome + ", cpf=" + cpf + ", email=" + email + ", valor=" + valor + ", parcelas="
				+ parcelas + ", seguro=" + seguro + "]";
	}
}