package model.bean;


public class ErrorResponse {
	
	private ErrorBean erros;
	
	public ErrorResponse() {
	}
	
	public ErrorResponse(ErrorBean erros) {
		super();
		this.erros = erros;
	}

	public ErrorBean geterros() {
		return erros;
	}

	public void seterros(ErrorBean erros) {
		this.erros = erros;
	}

	@Override
	public String toString() {
		return "ErrorResponse [erros=" + erros + "]";
	}
	
	
	
	
	
	

}
