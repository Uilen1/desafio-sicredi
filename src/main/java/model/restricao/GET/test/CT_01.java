package model.restricao.GET.test;

import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import io.restassured.response.Response;
import model.core.BaseTest;
import model.core.Constants;
import model.core.Properties;
import model.utilities.excel.DataDictionary;
import static io.restassured.RestAssured.*;
import static org.hamcrest.Matchers.*;

@RunWith(Parameterized.class)
public class CT_01 extends BaseTest {

	public CT_01(String executeTestName, DataDictionary excelData) {
		super(executeTestName, excelData);
	}

	@Parameters(name = "{0}")
	public static List<Object> parametersToTest() throws Exception {
		return loadData();
	}

	@Before
	public void beforeTest() {
		System.out.println(Constants.CABECALHO);
		System.out.println(Constants.DIRETORIO_RAIZ);
	}

	@Test
	public void Test() throws Exception {
		try {

			String[] valuesCpf = utils.returnArrayOfExcel((String) excelData.get("vCpfRestricao"));

			for (String value : valuesCpf) {
				
				utils.description("Testando com o CPF: " + value.trim());
				Response response = get("restricoes/" + value.trim());
				
				if(response.getStatusCode() == 200) {
					
					given().
						get("restricoes/" + value.trim())
					.then()
						.body("mensagem", equalToIgnoringCase("O CPF " + value.trim() + " tem problema"));
				
				} else {
					throw new Exception("N�o obteve-se a reposta 200 ao inv�s disso obteve-se: " + response.getStatusCode() );
				}

			}

		} catch (Exception e) {
			Properties.RESULT_TEST = e.getMessage();
			if (Properties.RESULT_TEST != "" || Properties.RESULT_TEST != null) {
				Properties.RESULT_TEST = "Failed - " + e.getMessage();
				System.out.println("[RESULT] = FAILED! \n");
				System.out.println(e.getMessage());
				throw new Exception(e.getMessage());
			}

		} finally {
			if (Properties.RESULT_TEST == "") {
				Properties.RESULT_TEST = "Passed";
				System.out.println("[RESULT] = SUCCESS! \n");
			}
		}

	}

}
