package model.simulacao.GET.test;

import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import io.restassured.response.Response;
import model.core.BaseTest;
import model.core.Constants;
import model.core.Properties;
import model.utilities.excel.DataDictionary;
import static io.restassured.RestAssured.*;

@RunWith(Parameterized.class)
public class CT_05 extends BaseTest {

	public CT_05(String executeTestName, DataDictionary excelData) {
		super(executeTestName, excelData);
	}

	@Parameters(name = "{0}")
	public static List<Object> parametersToTest() throws Exception {
		return loadData();
	}

	@Before
	public void beforeTest() {
		System.out.println(Constants.CABECALHO);
		System.out.println(Constants.DIRETORIO_RAIZ);
	}

	@Test
	public void Test() throws Exception {
		try {
			
			utils.description("Consultar todas a simula��es cadastradas");
			Response response = given().get("simulacoes/");
			List<String> jsonResponse = response.jsonPath().getList("$");
			
			if(response.getStatusCode() == 204) {
									
				throw new Exception("N�o obteve-se a reposta 204 ao inv�s disso obteve-se: " + response.getStatusCode() );
				
			} else if(response.getStatusCode() == 200 && jsonResponse.size() == 0) {
				
				throw new Exception("N�o h� nenhum elemento cadastrado, esperava-se o status 204 mas foi retornado: " + response.getStatusCode() );

			} else {
				
				System.out.println("Quantidade de itens cadastrados: " + jsonResponse.size());
				
			}

		} catch (Exception e) {
			Properties.RESULT_TEST = e.getMessage();
			if (Properties.RESULT_TEST != "" || Properties.RESULT_TEST != null) {
				Properties.RESULT_TEST = "Failed - " + e.getMessage();
				System.out.println("[RESULT] = FAILED! \n");
				System.out.println(e.getMessage());
				throw new Exception(e.getMessage());
			}

		} finally {
			if (Properties.RESULT_TEST == "") {
				Properties.RESULT_TEST = "Passed";
				System.out.println("[RESULT] = SUCCESS! \n");
			}
		}

	}

}
