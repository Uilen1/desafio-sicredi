package model.simulacao.POST.test;

import java.util.List;

import org.json.simple.JSONObject;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import io.restassured.http.ContentType;
import io.restassured.response.Response;
import model.core.BaseTest;
import model.core.Constants;
import model.core.Properties;
import model.utilities.excel.DataDictionary;
import static io.restassured.RestAssured.*;

@RunWith(Parameterized.class)
public class CT_13 extends BaseTest {

	public CT_13(String executeTestName, DataDictionary excelData) {
		super(executeTestName, excelData);
	}

	@Parameters(name = "{0}")
	public static List<Object> parametersToTest() throws Exception {
		return loadData();
	}

	@Before
	public void beforeTest() {
		System.out.println(Constants.CABECALHO);
		System.out.println(Constants.DIRETORIO_RAIZ);
	}

	@Test
	public void Test() throws Exception {
		try {

			String valueCpf = (String) excelData.get("vCpfCadastradoSimulacao");
			JSONObject jsonObject =  utils.createJsonFile((String) excelData.get("vNome"), (String) excelData.get("vCpfCadastradoSimulacao"), (String) excelData.get("vEmail"), (String) excelData.get("vValor"), (String) excelData.get("vParcela"), (String) excelData.get("vSeguro"));
			utils.description("Criando um registro de simula��o para CPF: " + valueCpf.trim());
			Response response = 
					given()
						.header("Content-Type", "application/json")
						.contentType(ContentType.JSON)
						.accept(ContentType.JSON)
						.body(jsonObject)
					.when()
						.post("simulacoes");

			if (!(response.getStatusCode() == 201)) {

				throw new Exception("N�o obteve-se a reposta 201 ao inv�s disso obteve-se: " + response.getStatusCode());

			}

		} catch (Exception e) {
			Properties.RESULT_TEST = e.getMessage();
			if (Properties.RESULT_TEST != "" || Properties.RESULT_TEST != null) {
				Properties.RESULT_TEST = "Failed - " + e.getMessage();
				System.out.println("[RESULT] = FAILED! \n");
				System.out.println(e.getMessage());
				throw new Exception(e.getMessage());
			}

		} finally {
			if (Properties.RESULT_TEST == "") {
				Properties.RESULT_TEST = "Passed";
				System.out.println("[RESULT] = SUCCESS! \n");
			}
		}

	}

}
