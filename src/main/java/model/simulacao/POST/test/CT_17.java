package model.simulacao.POST.test;

import java.util.List;

import org.json.simple.JSONObject;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;


import io.restassured.http.ContentType;
import io.restassured.response.Response;
import model.bean.ErrorResponse;
import model.core.BaseTest;
import model.core.Constants;
import model.core.Properties;
import model.utilities.excel.DataDictionary;
import static io.restassured.RestAssured.*;

@RunWith(Parameterized.class)
public class CT_17 extends BaseTest {

	public CT_17(String executeTestName, DataDictionary excelData) {
		super(executeTestName, excelData);
	}

	@Parameters(name = "{0}")
	public static List<Object> parametersToTest() throws Exception {
		return loadData();
	}

	@Before
	public void beforeTest() {
		System.out.println(Constants.CABECALHO);
		System.out.println(Constants.DIRETORIO_RAIZ);
	}

	@Test
	public void Test() throws Exception {
		try {

			String valueCpf = (String) excelData.get("vCpfCadastradoSimulacao");
			JSONObject jsonObject =  utils.createJsonFile((String) excelData.get("vNome"));
			utils.description("Criando o registro pelo CPF: " + valueCpf.trim());
			Response response = (Response) 
				given()
					.header("Content-Type", "application/json")
					.contentType(ContentType.JSON)
					.accept(ContentType.JSON)
					.body(jsonObject)
				.when()
					.post("simulacoes");		

			
			if (response.getStatusCode() == 400) {
				
				ErrorResponse errorResponse = response.body().as(ErrorResponse.class);
				
				if(errorResponse.geterros().getParcelas() == null) {
					throw new Exception("N�o foi retornado a mensagem para a regra de Parcelas Campo Obrigat�rio!");
				} else {
					if(!errorResponse.geterros().getParcelas().contentEquals("Parcelas n�o pode ser vazio")) {
						throw new Exception("A mensagem retornada pela API '"+errorResponse.geterros().getParcelas()+"' n�o est� de acordo com o Esperado: 'Parcelas n�o pode ser vazio'" );
					}
				}
				
				if(errorResponse.geterros().getEmail() == null) {
					throw new Exception("N�o foi retornado a mensagem para a regra de Email Campo Obrigat�rio!");
				} else {
					if(!errorResponse.geterros().getEmail().contentEquals("E-mail n�o deve ser vazio")) {
						throw new Exception("A mensagem retornada pela API '"+errorResponse.geterros().getEmail()+"' n�o est� de acordo com o Esperado: 'E-mail n�o deve ser vazio'" );
					}
				}
				
				if(errorResponse.geterros().getValor() == null) {
					throw new Exception("N�o foi retornado a mensagem para a regra de Valores Campo Obrigat�rio!");
				} else {
					if(!errorResponse.geterros().getValor().contentEquals("Valor n�o pode ser vazio")) {
						throw new Exception("A mensagem retornada pela API '"+errorResponse.geterros().getValor()+"' n�o est� de acordo com o Esperado: 'Valor n�o pode ser vazio'" );
					}
				}
				
				if(errorResponse.geterros().getCpf() == null) {
					throw new Exception("N�o foi retornado a mensagem para a regra de CPF Campo Obrigat�rio!");
				} else {
					if(!errorResponse.geterros().getCpf().contentEquals("CPF n�o pode ser vazio")) {
						throw new Exception("A mensagem retornada pela API '"+errorResponse.geterros().getValor()+"' n�o est� de acordo com o Esperado: 'CPF n�o pode ser vazio'" );
					}
				}
				
				if(errorResponse.geterros().getNome() == null) {
					throw new Exception("N�o foi retornado a mensagem para a regra de Nome Campo Obrigat�rio!");
				} else {
					if(!errorResponse.geterros().getNome().contentEquals("Nome n�o pode ser vazio")) {
						throw new Exception("A mensagem retornada pela API '"+errorResponse.geterros().getValor()+"' n�o est� de acordo com o Esperado: 'Nome n�o pode ser vazio'" );
					}
				}
				
				if(errorResponse.geterros().getSeguro() == null) {
					throw new Exception("N�o foi retornado a mensagem para a regra de Seguro Campo Obrigat�rio!");
				} else {
					if(!errorResponse.geterros().getSeguro().contentEquals("Seguro n�o pode ser vazio")) {
						throw new Exception("A mensagem retornada pela API '"+errorResponse.geterros().getValor()+"' n�o est� de acordo com o Esperado: 'Seguro n�o pode ser vazio'" );
					}
				}
				
			}else {
				throw new Exception("N�o obteve-se a reposta 400 para Campos Obrigat�rios n�o preenchidos ao inv�s disso obteve-se: " + response.getStatusCode() + "\n" + response.getBody().prettyPrint());
			}

		} catch (Exception e) {
			Properties.RESULT_TEST = e.getMessage();
			if (Properties.RESULT_TEST != "" || Properties.RESULT_TEST != null) {
				Properties.RESULT_TEST = "Failed - " + e.getMessage();
				System.out.println("[RESULT] = FAILED! \n");
				System.out.println(e.getMessage());
				throw new Exception(e.getMessage());
			}

		} finally {
			if (Properties.RESULT_TEST == "") {
				Properties.RESULT_TEST = "Passed";
				System.out.println("[RESULT] = SUCCESS! \n");
			}
		}

	}

}
