package model.simulacao.PUT.test;

import java.util.List;

import org.json.simple.JSONObject;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;


import io.restassured.http.ContentType;
import io.restassured.response.Response;
import model.bean.ErrorResponse;
import model.core.BaseTest;
import model.core.Constants;
import model.core.Properties;
import model.utilities.excel.DataDictionary;
import static io.restassured.RestAssured.*;

@RunWith(Parameterized.class)
public class CT_12 extends BaseTest {

	public CT_12(String executeTestName, DataDictionary excelData) {
		super(executeTestName, excelData);
	}

	@Parameters(name = "{0}")
	public static List<Object> parametersToTest() throws Exception {
		return loadData();
	}

	@Before
	public void beforeTest() {
		System.out.println(Constants.CABECALHO);
		System.out.println(Constants.DIRETORIO_RAIZ);
	}

	@Test
	public void Test() throws Exception {
		try {

			String valueCpf = (String) excelData.get("vCpfCadastradoSimulacao");
			JSONObject jsonObject =  utils.createJsonFile((String) excelData.get("vNome"), (String) excelData.get("vCpfCadastradoSimulacao"), (String) excelData.get("vEmail"), (String) excelData.get("vValor"), (String) excelData.get("vParcela"), (String) excelData.get("vSeguro"));
			utils.description("Alterando o registro pelo CPF: " + valueCpf.trim());
			Response response = (Response) 
				given()
					.header("Content-Type", "application/json")
					.contentType(ContentType.JSON)
					.accept(ContentType.JSON)
					.body(jsonObject)
				.when()
					.put("simulacoes/" + valueCpf.trim());		

			
			if (response.getStatusCode() == 400) {
				
				ErrorResponse errorResponse = response.body().as(ErrorResponse.class);
				
				if(errorResponse.geterros().getParcelas() == null) {
					throw new Exception("N�o foi retornado a mensagem para a regra de Parcelas inv�lidas!");
				} else {
					if(!errorResponse.geterros().getParcelas().contentEquals("Parcelas deve ser igual ou maior que 2")) {
						throw new Exception("A mensagem retornada pela API '"+errorResponse.geterros().getParcelas()+"' n�o est� de acordo com o Esperado: '" + "Parcelas deve ser igual ou maior que 2'" );
					}
				}
				
				if(errorResponse.geterros().getEmail() == null) {
					throw new Exception("N�o foi retornado a mensagem para a regra de Email inv�lido!");
				} else {
					if(!errorResponse.geterros().getEmail().contentEquals("E-mail deve ser um e-mail v�lido")) {
						throw new Exception("A mensagem retornada pela API '"+errorResponse.geterros().getEmail()+"' n�o est� de acordo com o Esperado: 'E-mail deve ser um e-mail v�lido'" );
					}
				}
				
				if(errorResponse.geterros().getValor() == null) {
					throw new Exception("N�o foi retornado a mensagem para a regra de Valores inv�lidos!");
				} else {
					if(!errorResponse.geterros().getValor().contentEquals("Valor deve ser igual ou maior que R$ 1.000")) {
						throw new Exception("A mensagem retornada pela API '"+errorResponse.geterros().getValor()+"' n�o est� de acordo com o Esperado: '" + "Valor deve ser igual ou maior que R$ 1.000'" );
					}
				}
				
				if(errorResponse.geterros().getCpf() == null) {
					throw new Exception("N�o foi retornado a mensagem para a regra de CPF no formato inv�lido!");
				} else {
					if(!errorResponse.geterros().getCpf().contentEquals("CPF n�o no formato 999.999.999-99")) {
						throw new Exception("A mensagem retornada pela API '"+errorResponse.geterros().getValor()+"' n�o est� de acordo com o Esperado: '" + "CPF n�o no formato 999.999.999-99'" );
					}
				}
				
			}else {
				throw new Exception("N�o obteve-se a reposta 400 para Dados incorretos ao inv�s disso obteve-se: " + response.getStatusCode() + "\n" + response.getBody().prettyPrint());
			}

		} catch (Exception e) {
			Properties.RESULT_TEST = e.getMessage();
			if (Properties.RESULT_TEST != "" || Properties.RESULT_TEST != null) {
				Properties.RESULT_TEST = "Failed - " + e.getMessage();
				System.out.println("[RESULT] = FAILED! \n");
				System.out.println(e.getMessage());
				throw new Exception(e.getMessage());
			}

		} finally {
			if (Properties.RESULT_TEST == "") {
				Properties.RESULT_TEST = "Passed";
				System.out.println("[RESULT] = SUCCESS! \n");
			}
		}

	}

}
