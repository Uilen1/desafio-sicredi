# 1. desafio-sicredi

Desafio QA Automação

## 1.1. Sobre a Estrutura do código

A estrutura do código está apresentada nas imagens abaixo:

![image](/uploads/328b504f6d49fdf52ec33696847ec557/image.png)
![image](/uploads/07cc084ad2582745fdbfdbf100b7b0b6/image.png)

<strong>OBS: A parte que está selecionada em vermelho na imagem, indica o caminho para os casos de teste.

## 1.2. Inputs e OutPuts para o teste

Para o controle dos Inputs e OutPuts para cada caso de teste, foi elaborado uma planilha, a qual é apresentada um esboço abaixo:

![image](/uploads/f90d21f446c1d3a73556280382ed1b8b/image.png)

<strong>Path: '(diretorioRaizProjeto)/data/test.xlsx
<p>Essa imagem é apenas um exemplo de um caso de teste específico, mas a mesma contém muitas outras linhas, para os demais Ct's<\p>

### 1.2.1. Breve abordagem sobre a semântica de cada coluna:

<p>=> Test: Apresenta a Rastreabilidade para a identificação de um caso específico de teste;<\p>
<p>=> RunTest: Indica qual massa estará habilitada para ser executada:<\p>
    <p>EX: Deve selecionar como 'Yes' as massas de dados na planilha<\p>
<p>=> Status: É aonde será indicado se o teste foi aprovado (Passed) ou reprovado (Failed + Motivo da falha)<\p>
<p>=> Variáveis de Entrada: São apresentadas pela notação (vNomeVariavel) EX: 'vCpfRestricao'<\p>
<p>=> Variáveis de Saída: São apresentadas pela notação (vOutNomeVariavel) EX: 'vOutData'<\p>

### 1.2.2. Execução dos casos de teste:
Para executar os Casos de teste, é necessário:
<p> 1. Configurar na planilha de variáveis, qual caso de teste deseja-se executar;<\p>
<p> 1.2. Fechar a Planilha, caso contrário é lançado uma exceção, pois é executado leitura e escrita na mesma; <\p>
<p> 1.3. Executar o caso de Teste Desejado na IDE.<\p>
